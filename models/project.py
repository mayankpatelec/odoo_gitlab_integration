# -*- coding: utf-8 -*-
import logging
import gitlab
from odoo import api, exceptions, fields, models, tools
from odoo.tools.translate import _

from odoo.exceptions import Warning, ValidationError

_logger = logging.getLogger(__name__)


class Project(models.Model):
    _inherit = 'project.project'

    gitlab_id = fields.Integer('GitLab ID')
    gitlab_group_id = fields.Many2one(
        'project.gitlab.group', 'Group')
    tag_ids = fields.Many2many(
        'project.gitlab.tag', 'project_gitlab_tag_rel', 'project_id', 'tag_id',
        string='Tags')
    
    @api.multi
    def write(self, vals):
        context = self._context.copy()
        ProjectTags_pool = self.env['project.gitlab.tag']
        if vals.get('name', False) or vals.get('tag_ids', False)  and 'gitlab' not in context:
            gl = gitlab.Gitlab('https://gitlab.com', self.env.user.gitlab_token)
            for project in self:
                if project.gitlab_id:
                    # Find related Gitlab Project
                    project_gitlab = gl.projects.get(project.gitlab_id)
                    # Sync Project Name and Tags
                    if vals.get('name', False):
                        project_gitlab.name = vals.get('name', False)
                    if vals.get('tag_ids', False) and len(vals.get('tag_ids')[0]) == 3:
                        project_tags = project_gitlab.tag_list
                        for tag in ProjectTags_pool.browse(vals.get('tag_ids')[0][2]):
                            if tag.name not in project_gitlab.tag_list:
                                project_tags.append(tag.name)
                        project_gitlab.tag_list = project_tags
                    project_gitlab.save()
        super(Project, self).write(vals)
        if vals.get('favorite_user_ids', False) and 'gitlab' not in context:
            for project in self:
                gl = gitlab.Gitlab('https://gitlab.com', self.env.user.gitlab_token)
                project_gitlab = gl.projects.get(project.gitlab_id)
                # Sync Project Members
                for m in project.favorite_user_ids:
                    if m.gitlab_id:
                        project_gitlab.favorite_user_ids.create({'user_id': m.gitlab_id, 'access_level':
                            gitlab.DEVELOPER_ACCESS})
        return True


class ProjectTask(models.Model):
    _inherit = 'project.task'

    gitlab_id = fields.Integer('GitLab ID')
    user_ids = fields.Many2many(
        'res.users', 'res_user_task_rel', 'task_id', 'user_id',
        string='Assigned to')

    states = {
        'Done': 'closed',
        'Cancelled': 'closed',
    }

    @api.multi
    def write(self, vals):
        Project_pool = self.env['project.project']
        ProjectTaskType_pool = self.env['project.task.type']
        context = self._context.copy()
        headers = {
            'PRIVATE-TOKEN': self.env.user.gitlab_token,
        }
        gl = gitlab.Gitlab('https://gitlab.com', self.env.user.gitlab_token)
        for task in self:
            if 'gitlab' not in context:
                try:
                    issue = False
                    if not vals.get('stage_id') and task.stage_id and task.stage_id.closed_issue:
                        # Skip sync for Closed task
                        continue
                    if task.project_id and task.project_id.gitlab_id and task.gitlab_id:
                        issue = gl.project_issues.get(task.gitlab_id, project_id=task.project_id.gitlab_id)
                        # Update Name to Gitlab
                        name = vals.get('name', False)
                        if name:
                            issue.title = name
                        # Update Desc to Gitlab
                        description = vals.get('description', False)
                        if description:
                            issue.description = description
                        # Update Stage to Gitlab
                        stage_id = vals.get('stage_id', False)
                        if stage_id:
                            temp = ProjectTaskType_pool.browse(vals['stage_id'])
                            issue.state = self.states.get(temp.name, 'opened')
                        # Update Deadline Date to Gitlab
                        date_deadline = vals.get('date_deadline', False)
                        if date_deadline:
                            issue.due_date = date_deadline
                        issue.save()
                    elif not task.gitlab_id and task.project_id and task.project_id.gitlab_id:
                        # Create Issue Odoo to Gitlab 
                        title = vals.get('name') or task.name
                        description = vals.get('description') or task.description
                        iss_vals = {
                            'title': title,
                            'description': description,
                            }
                        issue = gl.projects.get(task.project_id.gitlab_id).issues.create(iss_vals)
                        if issue:
                            vals.update({'gitlab_id': issue.id})
                    elif not task.gitlab_id and not task.project_id and vals.get('project_id'):
                        # Create Issue Odoo to Gitlab 
                        title = vals.get('name') or task.name
                        description = vals.get('description') or task.description
                        iss_vals = {
                            'title': title,
                            'description': description,
                            }
                        project_obj = Project_pool.browse(vals.get('project_id'))
                        if project_obj.gitlab_id:
                            issue = gl.projects.get(project_obj.gitlab_id).issues.create(iss_vals)
                            if issue:
                                vals.update({'gitlab_id': issue.id})
                    # Update Project Change In Issue
                    if task.project_id and vals.get('project_id') and issue:
                        project_obj = Project_pool.browse(vals.get('project_id'))
                        if project_obj.gitlab_id:
                            issue.move(project_obj.gitlab_id)
                            issue.save()
                except:
                    print "GitlabAuthenticationError"

        result = super(ProjectTask, self).write(vals)
        try:
            for task in self:
                if task.stage_id and task.stage_id.closed_issue:
                    # Skip sync for Closed Task
                    continue
                issue = gl.project_issues.get(task.gitlab_id, project_id=task.project_id.gitlab_id)
                label_list = issue.labels
                for label in task.categ_ids:
                    if label.name not in issue.labels:
                        label_list.append(label.name)
                # Update Stage in Labels
                if task.stage_id.gitlab_issue_label not in issue.labels:
                    label_list.append(task.stage_id.gitlab_issue_label)
                if label_list:
                    issue.labels = label_list
                issue.save()
        except:
            print "GitlabAuthenticationError"
        return result
        
    @api.model
    def create(self, vals):
        Project_pool = self.env['project.project']
        ctx = self.env.context.copy()
        if vals.get('project_id') and 'gitlab' not in ctx:
            user = self.env.user
            gl = gitlab.Gitlab('https://gitlab.com', user.gitlab_token)
            project_obj = Project_pool.browse(vals.get('project_id'))
            # Issue create Odoo to Gitlab
            if project_obj.gitlab_id:
                iss_vals = {
                    'title': vals.get('name'),
                    'description': vals.get('description'),
                    }
                issue = gl.projects.get(project_obj.gitlab_id).issues.create(iss_vals)
                if issue:
                    vals.update({'gitlab_id': issue.id})
        return super(ProjectTask, self).create(vals)


class ProjectTaskType(models.Model):
    _inherit = 'project.task.type'
    
    closed_issue = fields.Boolean('Closed Issue')
    gitlab_issue_label = fields.Char('Gitlab Issue Label')
    
    @api.one
    @api.constrains('closed_issue')
    def _check_closed_issue(self):
        if len(self.search([('closed_issue', '=', True)])) > 1:
            raise ValidationError(_('Only one stage can be Closed Issue stage.'))
            

class ProjectGitlabGroup(models.Model):
    _name = 'project.gitlab.group'

    name = fields.Char('Group', required=True)
    gitlab_id = fields.Integer('GitLab ID')
    description = fields.Text('Description')
    user_ids = fields.Many2many(
        'res.users', 'res_user_group_rel', 'task_id', 'user_id',
        string='Members')

    @api.multi
    def write(self, vals):
        res = super(ProjectGitlabGroup, self).write(vals)
        if vals.get('user_ids'):
            user = self.env.user
            gl = gitlab.Gitlab('https://gitlab.com', user.gitlab_token)
            group = gl.groups.get(self.gitlab_id)
            members = group.members.list()
            g_members = [m.id for m in members]
            u_list = [uid1.gitlab_id for uid1 in self.user_ids]
            del_list = list(set(g_members)-set(u_list))
            for uid in self.user_ids:
                if uid.gitlab_id not in g_members:
                    group.members.create({'user_id': uid.gitlab_id,
                                          'access_level': gitlab.DEVELOPER_ACCESS})
            for del1 in del_list:
                group.members.delete(del1)
        return res


class ProjectGitlabTag(models.Model):
    _name = 'project.gitlab.tag'

    name = fields.Char('Tag name', required=True)
    note = fields.Text('Release notes')
    message = fields.Text('Message')
